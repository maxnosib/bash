#!/bin/sh

# создаем временную папку
mkdir tmp && cd tmp
# устанавливаем chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm && yum localinstall google-chrome-stable_current_x86_64.rpm -y
# устанавливаем	git 
yum -y install https://packages.endpoint.com/rhel/7/os/x86_64/endpoint-repo-1.7-1.x86_64.rpm && yum install git -y && git --version
# устанавливаем vs code
rpm --import https://packages.microsoft.com/keys/microsoft.asc && 
echo "[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" >> /etc/yum.repos.d/vscode.repo &&
yum install code -y
# устанавливаем mc и nano
yum -y install mc && yum -y install nano
# настраиваем nano
echo "set nowrap" >>/etc/nanorc &&
cat <<EOF >>/etc/profile.d/nano.sh
export VISUAL="nano"
export EDITOR="nano"
EOF
# установка ovpn клиента для gnome 
yum -y install NetworkManager-openvpn NetworkManager-openvpn-gnome
# установка dbeaver
wget https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm &&
rpm -Uvh ./dbeaver-ce-latest-stable.x86_64.rpm
# удаляем tmp
cd ../ && rm -r -f tmp