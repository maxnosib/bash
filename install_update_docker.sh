#!/bin/sh

echo "удаляем старую версию?"
select yn in "Yes" "No"; do
    case $yn in
        # удаляем старую версию
        Yes ) is_first=true; break;;
        # нет это первая установка
        No  ) break;;
    esac
done


if [ $is_first ];
then
    echo "останавливаем docker"
    sudo systemctl stop docker.service

    echo "удаление старой версии"
    sudo yum remove -y docker-ce docker-ce-cli

    echo "удаляем изображения, контейнеры и тома"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) sudo rm -rf /var/lib/docker; break;;
            No  ) break;;
        esac
    done

    echo "удаляем файлы конфига"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) sudo rm -rf /etc/docker; break;;
            No  )  break;;
        esac
    done

fi

echo "нужно ли устанавливать docker"
select yn in "Yes" "No"; do
    case $yn in
        # продолжаем
        Yes )  echo "установка docker-ce"; break;;
        # выходим
        No  )  exit 0; break;;
    esac
done

# устанавливаем необходимые зависимости
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

# настраиваем стабильный Docker-репозиторий
sudo yum-config-manager -y --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# естанавливаем Docker
sudo yum install -y docker-ce

# чтобы не пришлось набирать префикс sudo каждый раз
# когда нужно запустить docker
# добавим своего пользователя в группу docker
sudo usermod -aG docker $(whoami)

# добавим Docker в автозагрузку
sudo systemctl enable docker.service

# запускаем Docker
sudo systemctl start docker.service

# проверяем что все установилось
sudo docker run hello-world



echo "нужно ли устанавливать docker-compose"
select yn in "Yes" "No"; do
    case $yn in
        # продолжаем
        Yes )  echo "установка docker-compose"; break;;
        # выходим
        No  )  exit 0; break;;
    esac
done

# устанавливаем необходимые зависимости
sudo yum install -y epel-release

# устанавливаем python-pip
sudo yum install -y python-pip
sudo pip install --upgrade pip

# устанавливаем docker-compose
sudo pip install docker-compose --ignore-installed

# обновляем пакеты Python на CentOS 7, чтобы обеспечить успешную работу docker-compose
sudo yum upgrade -y python*

# проверяем успешность установки
docker-compose version









