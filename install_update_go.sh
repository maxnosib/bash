#!/bin/sh
echo "это первая установка"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) is_first=true; break;;
        # удаляем старую версию
        No  ) echo "удаляем старую"; sudo rm -rf /usr/local/go; break;;
    esac
done

regex="^1\.[1-9]?[0-9]?\.?[1-9]?[0-9]?$"
echo "введите нужную версию golang:"
echo "ex: 1.1.1"
read version_go


if [[ $version_go =~ $regex ]]; 
then
    echo "скачиваем "$version_go".linux-amd64.tar.gz версию golang:"
    CURL_URL="https://storage.googleapis.com/golang/go"$version_go".linux-amd64.tar.gz"
    curl -O $CURL_URL
    
    echo "расспаковываем "$version_go".linux-amd64.tar.gz в /usr/local:"
    tar_name="go"$version_go".linux-amd64.tar.gz"
    # устанавливаем го в нужное место
    sudo tar -C /usr/local -xzf $tar_name
    # удаляем архив
    rm $tar_name
    if [ $is_first ];then
        # создаем необходимые папки для функционирования go
        echo "создаем необоходимую структуру папок"
        mkdir -p $HOME/go/bin
        mkdir $HOME/go/src
        mkdir $HOME/go/pkg
        # дописываем необходимый экспорт переменных окружения
        echo "настраиваем GOTATH"
        echo "export GOBIN="$HOME"/go/bin" >> $HOME/.bashrc
        echo "export GOPATH="$HOME"/go" >> $HOME/.bashrc
        echo "export PATH=\$PATH:"$HOME"/go/bin:/usr/local/go/bin" >> $HOME/.bashrc
        # перезагружаем переменные консоли
        source ~/.bashrc
        echo "~~~~~~~~~приятного использования~~~~~~~~~"
    fi
else
    echo "неверная версия: "$version_go
fi
# проверяем версию
echo $(go version)

